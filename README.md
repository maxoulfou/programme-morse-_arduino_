# TP Sin 2018 - Morse en arduino

TP Arduino pour la transmission de message en toutes lettres en morse.

- [x] Codes source Arduino (.ino)
- [x] Schéma électrique (Proteus)

*****

- [x] Codes Arduino commentés

*****

<img src = "https://gitlab.com/maxoulfou/programme-morse-_arduino_/raw/master/Images/Morse.PNG" title = "schéma" alt = "Morse Schema">

© Maxence Brochier 2018 - 2019.